package classes;

import java.util.Random;
import java.io.*; 

public class Laugh {
	private int type;
	private int style;
	private long length;
	
	//Getters:
	public int getType() {
		return type;
	}
	public int getStyle() {
		return style;
	}
	public long getLength() {
		return length;
	}
	
	//Setters:
	public void setType(int type) {
		this.type = type;
	}
	public void setStyle(int style) {
		this.style = style;
	}
	public void setLength(long length) {
		this.length = length;
	}	
	
	public void generateLaugher() {
		boolean randomType = false;
		String type;		
		switch (this.getType()) {
		case 1:
			type = "k";
			break;
		case 2:
			type = "ha";
			break;
		case 3:
			type = "rs";
			break;
		case 4:
			randomType = true;	
			type = "";
			break;
		default:			
			throw new IllegalArgumentException("Unexpected value: " + this.getType());
		}
		
		try {
			PrintStream fileOut = new PrintStream("laugh.txt");
			System.setOut(fileOut);		                              			
		} catch (FileNotFoundException ex) {
			System.setOut(System.out);
		}		

		for (int i = 1; i <= this.getLength(); i++) {
			if (randomType == true) {
				Random random = new Random(); 
				int number = random.nextInt(90 - 65) + 66;				
				char letter = (char)number;
				type = Character.toString(letter);
			}
			switch (this.getStyle()) {
			case 1:
				type = type.toLowerCase();
				break;
			case 2:
				type = type.toUpperCase();
				break;
			default:
				throw new IllegalArgumentException("Unexpected value: " + this.getStyle());
			}
			System.out.print(type);			
		}	
		
		try{
			java.awt.Desktop.getDesktop().open(new File("C:\\Users\\ghenriquedas\\eclipse-workspace\\laugh-generator\\laugh.txt")); 	    
		}
		catch(IOException e1){		 
			System.out.println(e1);
		}	
		
	}
}
