package main;

import java.util.Scanner;

import classes.Laugh;

public class Main {

	Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		Main main = new Main();
		main.defineLaugher();		
	}

	public void defineLaugher() {
		Laugh laugh = new Laugh();
		laugh.setType(typeMenu());
		laugh.setStyle(styleMenu());
		laugh.setLength(lengthMenu());
		laugh.generateLaugher();
	}	
	
	public int typeMenu() {
		System.out.println("Qual tipo de risada voc� quer gerar?");
		System.out.println("1 - kkk");
		System.out.println("2 - hahaha");
		System.out.println("3 - rsrsrs");
		System.out.println("4 - asldjalsdj (random)");
		return scanner.nextInt();		
	}
	
	public int styleMenu() {
		System.out.println("Qual o estilo de risada voc� quer gerar?");
		System.out.println("1 - Toda min�scula");
		System.out.println("2 - Toda mai�scula");
		//System.out.println("Aleat�rio");
		return scanner.nextInt();
	}
	
	public long lengthMenu() {
		System.out.println("Qual a quantidade de caracteres da risada que voc� quer gerar?");
		 return scanner.nextInt();		
	}

}
